package mundo;

import java.util.Date;

public class Trabajo extends Evento {

	public enum  TipoEventoTrabajo {
		JUNTA,
		ALMUERZO,
		CENA,
		COCTEL,
		PRESENTACION,
		OTRO_EVENTO_TRABAJO,
	}
	
	protected TipoEventoTrabajo tipo;
	
	public Trabajo(Date pFecha, String pLugar, TipoEventoTrabajo pTipo, boolean pObligatorio, boolean pFormal) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal);
		tipo = pTipo;  
	}

}
