package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 * @return entero que representa la fila en la cual terminó la jugada.
	 */
	public int registrarJugadaAleatoria()
	{
		Random randomGen = new Random();
		int col = randomGen.nextInt(tablero[0].length);
		return registrarJugada(col);
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return entero que representa la fila en la cual terminó la jugada.
	 */
	public int registrarJugada(int col)
	{
		int atIndex= jugadores.indexOf(atacante);
		int i = 0;
		while(!tablero[i][col].equals("___"))
		{
			i++;
		}
		tablero[i][col] = jugadores.get(atIndex).darSimbolo();
		if(turno == jugadores.size() -1)
			turno = 0;
		else
			turno++;
		atacante = jugadores.get(turno).darNombre();
		return i;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		int counter = 0;
		int counterDiagOne =0;
		int counterDiagTwo = 0;
		int filMod= fil;
		int colMod= col;
		String comp = tablero[fil][col];
		int numFil = tablero.length;
		int numCol = tablero[fil].length;
		boolean won = false;
		while(filMod>0)
		{
			if(comp.equals(tablero[filMod-1][col]))
				counter++;
			filMod --;
		}
		if(counter >= 4)
			won=true;
		counter = 0;
		filMod=fil;
		while(filMod < numFil -1 && !won)
		{
			if(comp.equals(tablero[filMod +1][col]))
				counter++;
			filMod++;
		}
		if(counter >= 4)
			won=true;
		counter = 0;
		while(colMod > 0 && !won)
		{
			if(comp.equals(tablero[fil][colMod-1]))
				counter++;
			colMod--;
		}
		colMod=col;
		if(counter >= 4)
			won=true;
		counter = 0;
		while(colMod < numCol -1 && !won)
		{
			if(comp.equals(tablero[fil][colMod +1]))
				counter++;
			colMod++;
		}
		filMod =fil;
		colMod =col;
		if(counter >= 4)
			won=true;
		while(colMod > 0 && !won)
		{
			if(filMod > 0)
			{
				if(comp.equals(tablero[filMod-1][colMod-1]))
					counterDiagOne++;
				filMod--;
			}
			colMod--;
		}
		colMod = col;
		filMod = fil;
		if(counterDiagOne >= 4)
			won = true;
		while(colMod < numCol-1 && !won)
		{
			if(filMod < numFil-1)
			{
				if(comp.equals(tablero[filMod+1][colMod+1]))
					counterDiagOne++;
				filMod++;
			}
			colMod++;
		}
		colMod = col;
		filMod = fil;
		if(counterDiagOne >= 4)
			won = true;
		while(colMod > 0 && !won)
		{
			if(filMod < numFil-1)
			{
				if(comp.equals(tablero[filMod+1][colMod-1]))
					counterDiagTwo++;
				filMod++;
			}
			colMod++;
		}
		colMod = col;
		filMod = fil;
		if(counterDiagTwo >= 4)
			won = true;
		while(colMod < numCol-1 && !won)
		{
			if(filMod > 0)
			{
				if(comp.equals(tablero[filMod-1][colMod+1]))
					counterDiagTwo++;
				filMod--;
			}
			colMod++;
		}
		if(counterDiagTwo >= 4)
			won=true;
		return won;
	}



}
