package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;
	
	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;
	
	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		imprimirMenu();
		while (true)
		{
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
    }
	
	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		ArrayList <Jugador> jugadores = new ArrayList<Jugador>();
		System.out.println("Ingrese cuántos jugadores habrán en la partida.");
		int numPlayers = Integer.parseInt(sc.next());
		for(int i = 1; i < numPlayers; i++)
		{
			System.out.println("Ingrese su nombre, jugador " + i);
			String nombrePlayer = sc.next();
			System.out.println("Ingrese su símbolo, jugador " + i);
			String symbol = "_" +sc.next() + "_";
			Jugador player = new Jugador(nombrePlayer, symbol);
			jugadores.add(player); 
		}
		System.out.println("Ingrese con cuántas filas deseas jugar.(Mínimo 4)");
		int fil = Integer.parseInt(sc.next());
		System.out.println("Ingrese con cuántas columnas deseas jugar. (Mínimo 4)");
		int col = Integer.parseInt(sc.next());
		juego = new LineaCuatro(jugadores, fil, col);
		juego();
	}
	
	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		imprimirTablero();
		System.out.println("Introduce en qué columna quieres jugar, jugador " + juego.darAtacante());
		int col = Integer.parseInt(sc.next());
		int fil = juego.registrarJugada(col);
		if(juego.terminar(fil, col))
		{
			System.out.println("¡Has ganado!");
			System.out.println("¿Deseas iniciar un nuevo juego? Ingresa Y (sí) o N (no).");
			if(sc.next() == "Y")
				empezarJuego();
		}
		else
		{
			juego();
		}
	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		ArrayList <Jugador> jugadores = new ArrayList<Jugador>();
		System.out.println("Ingrese su nombre");
		String nombrePlayer = sc.next();
		System.out.println("Ingrese su símbolo");
		String symbol = "_" +sc.next() + "_";
		Jugador player = new Jugador(nombrePlayer, symbol);
		Jugador ai = new Jugador("Maquina", "_G_");
		jugadores.add(player); jugadores.add(ai);
		System.out.println("Ingrese con cuántas filas deseas jugar.(Mínimo 4)");
		int fil = Integer.parseInt(sc.next());
		System.out.println("Ingrese con cuántas columnas deseas jugar. (Mínimo 4)");
		int col = Integer.parseInt(sc.next());
		juego = new LineaCuatro(jugadores, fil, col);
		juegoMaquina();
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		imprimirTablero();
		System.out.println("Introduce en qué columna quieres jugar.");
		int col = Integer.parseInt(sc.next());
		int fil = juego.registrarJugada(col);
		if(juego.terminar(fil, col))
		{
			System.out.println("¡Has ganado!");
			System.out.println("¿Deseas iniciar un nuevo juego? Ingresa Y (sí) o N (no).");
			if(sc.next() == "Y")
				empezarJuegoMaquina();
		}
		else
		{
			fil = juego.registrarJugadaAleatoria();
			if(juego.terminar(fil, col))
			{
				System.out.println("¡Has perdido!");
				System.out.println("¿Deseas iniciar un nuevo juego? Ingresa Y (sí) o N (no).");
				if(sc.next() == "Y")
					empezarJuegoMaquina();
			}
			juegoMaquina();
		}
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String tablero[][] = juego.darTablero();
		for(int i = tablero.length -1;  i >= 0; i--)
		{
			String linea ="";
			for(int j = 0; i < tablero[i].length; j++)
			{
				linea = linea + tablero[i][j];
			}
			System.out.println(linea);
		}
	}
}
